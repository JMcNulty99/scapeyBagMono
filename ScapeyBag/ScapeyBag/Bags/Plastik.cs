﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace ScapeyBag.Bags
{
    class Plastik : Bag
    {
        public override void init(Vector3 pos, Vector3 vel, float grav_mult = 1, float drag_mult = 1, float jump_mult = 10, float speed_mult = 1, float _health = 100, BagType type = BagType.Plastik)
        {
            base.init(pos, vel, grav_mult, drag_mult, jump_mult, speed_mult, _health, type);
        }

        public override void tick(GameTime gameTime)
        {
            base.tick(gameTime);
        }

        public override void jump()
        {
            base.jump();
        }
    }
}
